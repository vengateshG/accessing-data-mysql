package com.example.accessingdatamysql;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer age;
	private Integer employee_id;
	private String employee_name;
	private Integer salary;
	private Integer year_of_experience;
	private String Project_name;

	

	public Integer getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(Integer employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Integer getYear_of_experience() {
		return year_of_experience;
	}

	public void setYear_of_experience(Integer year_of_experience) {
		this.year_of_experience = year_of_experience;
	}

	public String getProject_name() {
		return Project_name;
	}

	public void setProject_name(String project_name) {
		Project_name = project_name;
	}
}
